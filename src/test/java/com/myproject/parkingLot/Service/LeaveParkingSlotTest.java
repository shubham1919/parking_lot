package com.myproject.parkingLot.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class LeaveParkingSlotTest {
	
	CarParkingLotVo carVo = new CarParkingLotVo();
	LeaveParkingSlot leave = new LeaveParkingSlot();
	CreateParkingLot create = new CreateParkingLot();
	AllocateParkingSlot allocate = new AllocateParkingSlot();
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final String CREATE_PARKING_LOT = "create_parking_lot 6";
	
	@Test
	public void testLeaveParkingSlotParkingLotMissing() {
		String command = "Leave 1";
		String[] splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		leave.runCommand(splitString, carVo);
		assertEquals(Commands.CREATE_PARKING_LOT.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testLeaveParkingSlotWrongSlotNumber() {
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carVo);
		String leaveString = "Leave 0";
		String[] leaveSplitString = leaveString.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		leave.runCommand(leaveSplitString, carVo);
		assertEquals(Commands.LEAVE.getErrorMessage(), outContent.toString().trim());
		leaveString = "Leave 7";
		leaveSplitString = leaveString.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		assertEquals(Commands.LEAVE.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testLeaveParkingLotNumberFormatException() {
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carVo);
		String leaveString = "Leave 0s";
		String[] leaveSplitString = leaveString.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		leave.runCommand(leaveSplitString, carVo);
		assertEquals(Commands.LEAVE.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testLeaveParkingLotRemoveSlot() {
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carVo);
		List<String> cars = new ArrayList<>();
		cars.add("park KA-01-HH-1234 White");
		cars.add("park KA-01-HH-9999 White");
		cars.stream().forEach(car -> {
			String[] split = car.trim().split("\\s+");
			allocate.runCommand(split, carVo);
		});
		String leaveString = "Leave 1";
		String[] leaveSplitString = leaveString.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		leave.runCommand(leaveSplitString, carVo);
		assertEquals("Slot number 1 is free", outContent.toString().trim());
	}
}
