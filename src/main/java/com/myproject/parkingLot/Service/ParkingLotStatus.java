package com.myproject.parkingLot.Service;

import java.util.List;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.Car;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class ParkingLotStatus implements ParkingLotService{

	@Override
	public void runCommand(String[] command, CarParkingLotVo carParkingLotVo) {
		if(!carParkingLotVo.isParkingLotCreated()) {
			System.out.println(Commands.CREATE_PARKING_LOT.getErrorMessage());
			return;
		}
		List<Car> cars = carParkingLotVo.getCarsInParkingLot();
		if(cars == null) {
			System.out.println(Commands.STATUS.getErrorMessage());
			return;
		}
		System.out.print("Slot No. ");
		System.out.print("Registration No. ");
		System.out.print("Color\n");

		cars.stream().forEach(car -> {
			System.out.printf("%d ", car.getSlotNumber());
			System.out.printf("%s ", car.getRegistrationNumber());
			System.out.printf("%s\n", car.getColor());
		});
	}

}
