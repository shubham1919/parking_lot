package com.myproject.parkingLot.Handler;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.myproject.parkingLot.Service.SlotNumberForRegistrationNumber;
import com.myproject.parkingLot.Vo.CarParkingLotVo;
import com.myproject.parkingLot.handler.CommandHandler;

public class CommandHandlerTest {
	
	CarParkingLotVo carParkingLotVo = new CarParkingLotVo();
	CommandHandler commandHandler = new CommandHandler();
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	
	@BeforeEach
	public void beforeEach() {
		String command = "create_parking_lot 6";
		commandHandler.execute(command, carParkingLotVo);
		command = "park KA-01-BB-0001 Black";
		commandHandler.execute(command, carParkingLotVo);
		command = "park KA-01-HH-7777 Red";
		commandHandler.execute(command, carParkingLotVo);
	}

	@Test
	public void testCreateParkingLot() {
		String command = "create_parking_lot 6";
		System.setOut(new PrintStream(outContent));
		commandHandler.execute(command, carParkingLotVo);
		assertEquals("Created a parking lot with 6 slots", outContent.toString().trim());
	}
	
	@Test
	public void testAllocatePakingSlot() {
		String command = "park KA-01-HH-1234 White";
		System.setOut(new PrintStream(outContent));
		commandHandler.execute(command, carParkingLotVo);
		assertEquals("Allocated Slot Number: 3", outContent.toString().trim());
	}
	
	@Test
	public void testLeaveSlot() {
		String command = "leave 2";
		System.setOut(new PrintStream(outContent));
		commandHandler.execute(command, carParkingLotVo);
		assertEquals("Slot number 2 is free", outContent.toString().trim());
	}
	
	@Test
	public void testRegistrationNumberForCarsWithColor() {
		String command = "registration_numbers_for_cars_with_colour Black";
		System.setOut(new PrintStream(outContent));
		commandHandler.execute(command, carParkingLotVo);
		assertEquals("KA-01-BB-0001", outContent.toString().trim());
		
	}
	
	@Test
	public void testSlotNumberForRegistrationNumber() {
		String command = "slot_number_for_registration_number KA-01-BB-0001";
		System.setOut(new PrintStream(outContent));
		commandHandler.execute(command, carParkingLotVo);
		assertEquals("1", outContent.toString().trim());
		
	}
	
	@Test
	public void testSlotNumbersForCarsWithColor() {
		String command = "slot_numbers_for_cars_with_colour Black";
		System.setOut(new PrintStream(outContent));
		commandHandler.execute(command, carParkingLotVo);
		assertEquals("1", outContent.toString().trim());
		
	}
	
	@Test
	public void testWrongArguments() {
		String command = "slot_numbers_for_cars_with_colour Black White";
		System.setOut(new PrintStream(outContent));
		commandHandler.execute(command, carParkingLotVo);
		assertEquals("com.myproject.parkingLot.Exceptions.IncorrectCommandException: Command Entered is incorrect or Arguments passed are wrong", outContent.toString().trim());
		
	}
}
