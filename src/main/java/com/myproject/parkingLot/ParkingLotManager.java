package com.myproject.parkingLot;

public interface ParkingLotManager {
	
	public void readInputFromConsole();
	
	public void readInputFromFile(String fileName);
	
	public default boolean fileExist(String fileName) {
		return true;
	}
	
	public default boolean containsExtension(String fileName) {
		return true;
	}
}
