package com.myproject.parkingLot.Service;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class LeaveParkingSlot implements ParkingLotService{

	@Override
	public void runCommand(String[] command, CarParkingLotVo carParkingLotVo) {
		int slot = 0;
		if(!carParkingLotVo.isParkingLotCreated()) {
			System.out.println(Commands.CREATE_PARKING_LOT.getErrorMessage());
			return;
		}
		try {
			slot = Integer.parseInt(command[1]);
			if(!carParkingLotVo.isSlotValueCorrect(slot)) {
				System.out.println(Commands.LEAVE.getErrorMessage());
				return;
			}
		} catch(NumberFormatException e) {
			System.out.println(Commands.LEAVE.getErrorMessage());
			return;
		}
		carParkingLotVo.removeCarFromSlot(slot);
		System.out.printf("Slot number %d is free\n", slot);
	}

}
