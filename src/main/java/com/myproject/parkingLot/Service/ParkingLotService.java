package com.myproject.parkingLot.Service;

import com.myproject.parkingLot.Vo.CarParkingLotVo;

public interface ParkingLotService {
	
	public void runCommand(String[] command, CarParkingLotVo carParkingLotVo);
}
