package com.myproject.parkingLot.Service;


import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.Car;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class AllocateParkingSlot implements ParkingLotService{

	@Override
	public void runCommand(String[] command, CarParkingLotVo carParkingLotVo) {
		if(!carParkingLotVo.isParkingLotCreated()) {
			System.out.println(Commands.CREATE_PARKING_LOT.getErrorMessage());
			return;
		}
		String registrationNumber = command[1];
		String color = command[2];
		Car car = new Car(color, registrationNumber);
		int status = carParkingLotVo.parkCarIfSlotAvailable(car);
		if(status == 0) {
			System.out.println(Commands.PARK.getErrorMessage());
			return;
		}
		System.out.printf("Allocated Slot Number: %d\n", status);
	}
}
