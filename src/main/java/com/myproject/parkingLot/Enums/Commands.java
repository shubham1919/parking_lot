package com.myproject.parkingLot.Enums;

public enum Commands {
	
	EXIT("exit", 1, ""),
	CREATE_PARKING_LOT("create_parking_lot", 2, "Parking Lot has not been created yet"),
	PARK("park", 3, "Sorry, parking lot is full"),
	LEAVE("leave", 2, "Argument passed for leave is not valid"),
	STATUS("status", 1,"Sorry, parking lot is empty"),
	REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR("registration_numbers_for_cars_with_colour", 2, 
			"Not Found"),
	SLOT_NUMBERS_FOR_CARS_WITH_COLOUR("slot_numbers_for_cars_with_colour", 2, "Not Found"),
	SLOT_NUMBERS_FOR_REGISTRATION_NUMBER("slot_number_for_registration_number", 2, "Not Found");

	private String command;
	
	private int numberOfArguments;
	
	private String errorMessage;
	
	Commands(String command, int numberOfArguments, String errorMessage) {
		this.command = command;
		this.numberOfArguments = numberOfArguments;
		this.errorMessage = errorMessage;
	}
	
	public String getCommand() {
		return command;
	}
	
	public int getNumberOfArguments() {
		return numberOfArguments;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
}
