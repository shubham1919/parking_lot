package com.myproject.parkingLot.Service;


import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.Car;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class SlotNumberForRegistrationNumber implements ParkingLotService{

	@Override
	public void runCommand(String[] command, CarParkingLotVo carParkingLotVo) {
		if(!carParkingLotVo.isParkingLotCreated()) {
			System.out.println(Commands.CREATE_PARKING_LOT.getErrorMessage());
			return;
		}
		String registrationNumber = command[1];
		Car car = carParkingLotVo.getCarObjectForRegistrationNumber(registrationNumber);
		if(car == null) {
			System.out.println(Commands.SLOT_NUMBERS_FOR_REGISTRATION_NUMBER.getErrorMessage());
			return;
		}
		System.out.println(car.getSlotNumber());
	}

}
