package com.myproject.parkingLot.Service;

import java.util.List;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.Car;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class SlotNumbersForCarsWithColor implements ParkingLotService{

	@Override
	public void runCommand(String[] command, CarParkingLotVo carParkingLotVo) {
		if(!carParkingLotVo.isParkingLotCreated()) {
			System.out.println(Commands.CREATE_PARKING_LOT.getErrorMessage());
			return;
		}
		String color = command[1];
		List<Car> cars = carParkingLotVo.getCarObjectForColor(color);
		if(cars == null) {
			System.out.println(Commands.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR.getErrorMessage());
			return;
		}
		StringBuilder stringBuilder = new StringBuilder();
		cars.stream().forEach(car -> {
			stringBuilder.append(car.getSlotNumber());
			stringBuilder.append(", ");
		});
		stringBuilder.deleteCharAt(stringBuilder.length() - 2);
		System.out.println(stringBuilder.toString().trim());
	}

}
