package com.myproject.parkingLot.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class AllocateParkingSlotTest {

	CreateParkingLot create = new CreateParkingLot();
	CarParkingLotVo carParkingLotVo = new CarParkingLotVo();
	AllocateParkingSlot allocate = new AllocateParkingSlot();
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final String CREATE_PARKING_LOT = "create_parking_lot 1";
	
	@Test
	public void testAllocateParkingSlotParkingLotMissing() {
		String command = "park KA-01-HH-3141 Black";
		String[] splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		allocate.runCommand(splitString, carParkingLotVo);
		assertEquals(Commands.CREATE_PARKING_LOT.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testAllocateParkingSlotIsAvailable() {
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carParkingLotVo);
		String command = "park KA-01-HH-3141 Black";
		splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		allocate.runCommand(splitString, carParkingLotVo);
		assertEquals("Allocated Slot Number: 1", outContent.toString().trim());
	}
	
	@Test
	public void testAllocateParkingSlotNotAvailable() {
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carParkingLotVo);
		String park1 = "park KA-01-HH-3141 Black";
		String park2 = "park KA-01-HH-1234 White";
		splitString = park1.trim().split("\\s+");
		allocate.runCommand(splitString, carParkingLotVo);
		splitString = park2.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		allocate.runCommand(splitString, carParkingLotVo);
		assertEquals(Commands.PARK.getErrorMessage(), outContent.toString().trim());
	}
}
