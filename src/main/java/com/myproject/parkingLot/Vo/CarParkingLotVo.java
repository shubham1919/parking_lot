package com.myproject.parkingLot.Vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.myproject.parkingLot.Vo.ParkingLotVo;

public class CarParkingLotVo extends ParkingLotVo{
	
	private Map<Integer, Car> carBySlotMap;
	private Map<String, List<Car>> carByColorMap;
	private Map<String, Car> carByRegistrationNumberMap;

	public void intializeCarParkingLot(int capacity) {
		carBySlotMap = new HashMap<>();
		carByColorMap = new HashMap<>();
		carByRegistrationNumberMap = new HashMap<>();
		this.initialize(capacity);
	}
	
	
	public int parkCarIfSlotAvailable(Car car) {
		if(!this.isParkingLotFull()) {
			int availableSlot = this.getAvailableSlot();
			car.setSlotNumber(availableSlot);
			carBySlotMap.put(availableSlot, car);
			carByRegistrationNumberMap.put(car.getRegistrationNumber(), car);
			this.addInCarByColorMap(car);
			return availableSlot;
		}
		return 0;
	}
	
	public void removeCarFromSlot(int slot) {
		if(carBySlotMap.containsKey(slot)) {
			Car car = carBySlotMap.get(slot);
			carBySlotMap.remove(slot);
			carByRegistrationNumberMap.remove(car.getRegistrationNumber());
			this.removeFromCarByColorMap(car);
			this.setSlotAsAvailable(slot);
		}
	}
	
	public List<Car> getCarsInParkingLot(){
		if(!this.isParkingLotEmpty() && carBySlotMap.size() > 0) {
			return carBySlotMap.values().stream().collect(Collectors.toList());
		}
		return null;
	}
	
	public List<Car> getCarObjectForColor(String color){
		if(!this.isParkingLotEmpty() && carByColorMap.containsKey(color)) {
			return carByColorMap.get(color);
		}
		return null;
	}
	
	public Car getCarObjectForRegistrationNumber(String registrationNumber){
		if(!this.isParkingLotEmpty() && carByRegistrationNumberMap.containsKey(registrationNumber)) {
			return carByRegistrationNumberMap.get(registrationNumber);
		}
		return null;
	}
	
	private void addInCarByColorMap(Car car) {
		String key = car.getColor();
		if(carByColorMap.containsKey(key)) {
			List<Car> cars = carByColorMap.get(key);
			cars.add(car);
			return;
		}
		List<Car> newCarList = new ArrayList<>();
		newCarList.add(car);
		carByColorMap.put(key, newCarList);
	}
	
	private void removeFromCarByColorMap(Car car) {
		String key = car.getColor();
		if(carByColorMap.containsKey(key)) {
			List<Car> cars = carByColorMap.get(key);
			cars.remove(car);
			carByColorMap.put(key, cars);
		}
	}
	
}
