Software requirements

1) Java 8
2) Maven

How to run :

1) run the "parking_lot" file using this command './parking_lot <input-file-name>'. Input file name is an optional parameter :
	
	a) If you want to pass input file name, pass the full name, for e.g, input.txt. After you run, you can see
		output.	
	b) If you want to input from CLI, please dont add file name parameter. To exit CLI just type 'exit' command.