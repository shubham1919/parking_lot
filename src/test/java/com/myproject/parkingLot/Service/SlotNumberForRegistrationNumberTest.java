package com.myproject.parkingLot.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class SlotNumberForRegistrationNumberTest {
	CreateParkingLot create = new CreateParkingLot();
	CarParkingLotVo carParkingLotVo = new CarParkingLotVo();
	AllocateParkingSlot allocate = new AllocateParkingSlot();
	SlotNumberForRegistrationNumber slotNumberForRegistrationNumber =
			new SlotNumberForRegistrationNumber();
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final String CREATE_PARKING_LOT = "create_parking_lot 6";
	
	@Test
	public void testSlotNumberForRegistrationNumberParkingLotMissing() {
		String command = "slot_number_for_registration_number KA-01-HH-3141";
		String[] splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		slotNumberForRegistrationNumber.runCommand(splitString, carParkingLotVo);
		assertEquals(Commands.CREATE_PARKING_LOT.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testSlotNumberForRegistrationNumberNoCars() {
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carParkingLotVo);
		String command = "slot_number_for_registration_number KA-01-HH-3141";
		splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		slotNumberForRegistrationNumber.runCommand(splitString, carParkingLotVo);
		assertEquals(Commands.SLOT_NUMBERS_FOR_REGISTRATION_NUMBER.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testSlotNumberForRegistrationNumberCarsAvailable(){
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carParkingLotVo);
		String park1 = "park KA-01-HH-3141 Black";
		String park2 = "park KA-01-HH-1234 White";
		String[] park1SplitString = park1.trim().split("\\s+");
		allocate.runCommand(park1SplitString, carParkingLotVo);
		String[] park2SplitString = park2.trim().split("\\s+");
		allocate.runCommand(park2SplitString, carParkingLotVo);
		String command = "slot_number_for_registration_number KA-01-HH-3141";
		splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		slotNumberForRegistrationNumber.runCommand(splitString, carParkingLotVo);
		assertEquals("1", outContent.toString().trim());
	}
}
