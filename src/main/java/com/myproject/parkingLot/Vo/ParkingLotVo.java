package com.myproject.parkingLot.Vo;

import java.util.PriorityQueue;

public class ParkingLotVo {
	
	private int capacity;
	private int currentSize;
	private PriorityQueue<Integer> slotsQueue = new PriorityQueue<>();
	
	public void initialize(int capacity) {
		this.capacity = capacity;
		this.currentSize = 0;
		for(int i=1;i<=capacity;i++) {
			this.slotsQueue.add(i);
		}
	}
	
	public boolean isParkingLotCreated() {
		return capacity ==0 ? false : true;
	}
	
	public boolean isParkingLotFull() {
		return currentSize == capacity;
	}
	
	public boolean isParkingLotEmpty() {
		return currentSize == 0;
	}
	
	public int getAvailableSlot() {
		currentSize++;
		return this.slotsQueue.remove();
	}
	
	public void setSlotAsAvailable(int slot) {
		currentSize--;
		this.slotsQueue.add(slot);
	}
	
	public boolean isSlotValueCorrect(int slot) {
		return (slot < 1 || slot > capacity) ? false : true;
	}
}
