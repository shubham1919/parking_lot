package com.myproject.parkingLot.Vo;

public class Car {
	
	private int slotNumber;
	private String color;
	private String registrationNumber;
	
	public Car(String color, String registrationNumber) {
		this.color = color;
		this.registrationNumber = registrationNumber;
	}
	
	public void setSlotNumber(int slot) {
		this.slotNumber = slot;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public String getRegistrationNumber() {
		return this.registrationNumber;
	}
	
	public int getSlotNumber() {
		return this.slotNumber;
	}
}
