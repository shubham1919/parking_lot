package com.myproject.parkingLot.Exceptions;

public class IncorrectCommandException extends RuntimeException{
	
	public IncorrectCommandException(String errorMessage) {
		super(errorMessage);
	}
}
