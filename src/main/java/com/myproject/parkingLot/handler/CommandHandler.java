package com.myproject.parkingLot.handler;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Exceptions.IncorrectCommandException;
import com.myproject.parkingLot.Service.AllocateParkingSlot;
import com.myproject.parkingLot.Service.CreateParkingLot;
import com.myproject.parkingLot.Service.LeaveParkingSlot;
import com.myproject.parkingLot.Service.ParkingLotStatus;
import com.myproject.parkingLot.Service.RegistrationNumberForCarsWithColor;
import com.myproject.parkingLot.Service.SlotNumberForRegistrationNumber;
import com.myproject.parkingLot.Service.SlotNumbersForCarsWithColor;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class CommandHandler {
	
	public void execute(String command, CarParkingLotVo carParkingLotVo) {
		
		String[] splitString = command.trim().split("\\s+");
		int size = splitString.length;
		try {
		if(splitString[0].equals(Commands.CREATE_PARKING_LOT.getCommand())
				&& size == Commands.CREATE_PARKING_LOT.getNumberOfArguments()) {
			CreateParkingLot createParkingLot = new CreateParkingLot();
			createParkingLot.runCommand(splitString, carParkingLotVo);
		} else if(splitString[0].equals(Commands.PARK.getCommand()) 
				&& size == Commands.PARK.getNumberOfArguments()) {
			AllocateParkingSlot allocateParkingSlot = new AllocateParkingSlot();
			allocateParkingSlot.runCommand(splitString, carParkingLotVo);
		}else if(splitString[0].equals(Commands.LEAVE.getCommand())
				&& size == Commands.LEAVE.getNumberOfArguments()){
			LeaveParkingSlot leaveParkingSlot = new LeaveParkingSlot();
			leaveParkingSlot.runCommand(splitString, carParkingLotVo);
		}else if(splitString[0].equals(Commands.STATUS.getCommand())
				&& size == Commands.STATUS.getNumberOfArguments()) {
			ParkingLotStatus parkingLotStatus = new ParkingLotStatus();
			parkingLotStatus.runCommand(splitString, carParkingLotVo);
		}else if(splitString[0].equals(Commands.REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR.getCommand())
				&& size == Commands.REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR.getNumberOfArguments()) {
			RegistrationNumberForCarsWithColor registrationNumberForCarsWithColor =
						new RegistrationNumberForCarsWithColor();
			registrationNumberForCarsWithColor.runCommand(splitString, carParkingLotVo);
		}else if(splitString[0].equals(Commands.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR.getCommand())
				&& size==Commands.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR.getNumberOfArguments()) {
			SlotNumbersForCarsWithColor slotNumbersForCarsWithColor = new 
					SlotNumbersForCarsWithColor();
			slotNumbersForCarsWithColor.runCommand(splitString, carParkingLotVo);
		}else if(splitString[0].equals(Commands.SLOT_NUMBERS_FOR_REGISTRATION_NUMBER.getCommand())
				&& size==Commands.SLOT_NUMBERS_FOR_REGISTRATION_NUMBER.getNumberOfArguments()) {
			SlotNumberForRegistrationNumber slotNumberForRegistrationNumber = new 
					SlotNumberForRegistrationNumber();
			slotNumberForRegistrationNumber.runCommand(splitString, carParkingLotVo);
		}else {
			throw new IncorrectCommandException("Command Entered is incorrect or Arguments passed are wrong");
		}
		}catch(IncorrectCommandException e) {
			System.out.println(e);
		}
	}
}
