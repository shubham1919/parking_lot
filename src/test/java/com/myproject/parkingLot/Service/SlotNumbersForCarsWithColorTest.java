package com.myproject.parkingLot.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class SlotNumbersForCarsWithColorTest {
	
	CreateParkingLot create = new CreateParkingLot();
	CarParkingLotVo carParkingLotVo = new CarParkingLotVo();
	AllocateParkingSlot allocate = new AllocateParkingSlot();
	SlotNumbersForCarsWithColor slotNumbersForCarsWithColor = 
			new SlotNumbersForCarsWithColor();
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final String CREATE_PARKING_LOT = "create_parking_lot 6";
	
	@Test
	public void testSlotNumbersForCarsWithColorParkingLotMissing() {
		String command = "slot_numbers_for_cars_with_colour White";
		String[] splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		slotNumbersForCarsWithColor.runCommand(splitString, carParkingLotVo);
		assertEquals(Commands.CREATE_PARKING_LOT.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testSlotNumbersForCarsWithColorNoCars() {
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carParkingLotVo);
		String command = "slot_numbers_for_cars_with_colour White";
		splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		slotNumbersForCarsWithColor.runCommand(splitString, carParkingLotVo);
		assertEquals(Commands.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testSlotNumbersForCarsWithColorCarsAvailable(){
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carParkingLotVo);
		String park1 = "park KA-01-HH-3141 Black";
		String park2 = "park KA-01-HH-1234 White";
		String[] park1SplitString = park1.trim().split("\\s+");
		allocate.runCommand(park1SplitString, carParkingLotVo);
		String[] park2SplitString = park2.trim().split("\\s+");
		allocate.runCommand(park2SplitString, carParkingLotVo);
		String command = "slot_numbers_for_cars_with_colour White";
		splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		slotNumbersForCarsWithColor.runCommand(splitString, carParkingLotVo);
		assertEquals("2", outContent.toString().trim());
	}
}
