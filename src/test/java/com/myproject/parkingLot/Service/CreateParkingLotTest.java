package com.myproject.parkingLot.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class CreateParkingLotTest {
	
	CreateParkingLot create = new CreateParkingLot();
	CarParkingLotVo carParkingLotVo = new CarParkingLotVo();
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	
	@Test
	public void testCreateParkingLotNumberFormatException() {
		String createString = "create_parking_lot s";
		String[] split = createString.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		create.runCommand(split, carParkingLotVo);
		assertEquals(Commands.LEAVE.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testCreateParkingLot() {
		String createString = "create_parking_lot 5";
		String[] split = createString.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		create.runCommand(split, carParkingLotVo);
		assertEquals("Created a parking lot with 5 slots", outContent.toString().trim());
	}
}
