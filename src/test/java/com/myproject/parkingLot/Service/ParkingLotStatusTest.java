package com.myproject.parkingLot.Service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.jupiter.api.Test;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class ParkingLotStatusTest {

	CreateParkingLot create = new CreateParkingLot();
	CarParkingLotVo carParkingLotVo = new CarParkingLotVo();
	AllocateParkingSlot allocate = new AllocateParkingSlot();
	ParkingLotStatus status = new ParkingLotStatus();
	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
	private final String CREATE_PARKING_LOT = "create_parking_lot 6";
	
	@Test
	public void testParkingLotStatusParkingLotMissing() {
		String command = "status";
		String[] splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		status.runCommand(splitString, carParkingLotVo);
		assertEquals(Commands.CREATE_PARKING_LOT.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testAllocateParkingSpotNoCar() {
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carParkingLotVo);
		String command = "status";
		splitString = command.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		status.runCommand(splitString, carParkingLotVo);
		assertEquals(Commands.STATUS.getErrorMessage(), outContent.toString().trim());
	}
	
	@Test
	public void testAllocateParkingSlotCars() {
		String[] splitString = CREATE_PARKING_LOT.trim().split("\\s+");
		create.runCommand(splitString, carParkingLotVo);
		String park1 = "park KA-01-HH-3141 Black";
		String park2 = "park KA-01-HH-1234 White";
		String[] park1SplitString = park1.trim().split("\\s+");
		allocate.runCommand(park1SplitString, carParkingLotVo);
		String[] park2SplitString = park2.trim().split("\\s+");
		allocate.runCommand(park2SplitString, carParkingLotVo);
		String statusCommand = "status";
		splitString = statusCommand.trim().split("\\s+");
		System.setOut(new PrintStream(outContent));
		status.runCommand(splitString, carParkingLotVo);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Slot No. ");
		stringBuilder.append("Registration No. ");
		stringBuilder.append("Color\n");
		stringBuilder.append("1 ");
		stringBuilder.append("KA-01-HH-3141 ");
		stringBuilder.append("Black\n");
		stringBuilder.append("2 ");
		stringBuilder.append("KA-01-HH-1234 ");
		stringBuilder.append("White");
		assertEquals(stringBuilder.toString(), outContent.toString().trim());
	}
}
