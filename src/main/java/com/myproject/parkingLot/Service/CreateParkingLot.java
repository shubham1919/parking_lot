package com.myproject.parkingLot.Service;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.CarParkingLotVo;

public class CreateParkingLot implements ParkingLotService{

	@Override
	public void runCommand(String[] command, CarParkingLotVo carParkingLotVo) {
		int capacity = 0;
		try {
			capacity = Integer.parseInt(command[1]);
		} catch(NumberFormatException e) {
			System.out.println(Commands.LEAVE.getErrorMessage());
			return;
		}
		carParkingLotVo.intializeCarParkingLot(capacity);
		System.out.printf("Created a parking lot with %d slots\n", capacity);
	}

}
