package com.myproject.parkingLot;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import com.myproject.parkingLot.Enums.Commands;
import com.myproject.parkingLot.Vo.CarParkingLotVo;
import com.myproject.parkingLot.handler.CommandHandler;

public class ParkingLotManagerImpl implements ParkingLotManager{
	
	CommandHandler commandHandler = new CommandHandler();
	CarParkingLotVo carParkingLotVo = new CarParkingLotVo();
	public void readInputFromConsole() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			String input = br.readLine();
			while(!(Commands.EXIT.getCommand()).equals(input)) {
				commandHandler.execute(input,carParkingLotVo);
				input = br.readLine();
			}
		}catch(IOException e) {
			System.out.println(e);
		}
	}
	
	public void readInputFromFile(String fileName) {
		try {
			File file = new File(fileName);
			Scanner scanner = new Scanner(file);
			while(scanner.hasNextLine()) {
				String nextLine = scanner.nextLine();
				commandHandler.execute(nextLine,carParkingLotVo);
			}
			scanner.close();
		}catch(FileNotFoundException e) {
			System.out.println(e);
		}
		
	}
}
