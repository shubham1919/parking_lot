package com.myproject.parkingLot;


public class ParkingLot 
{
    public static void main( String[] args )
    {
    	ParkingLotManagerImpl parkingLotManager = new ParkingLotManagerImpl();
    	if(args.length > 0) {
    		parkingLotManager.readInputFromFile(args[0]);
    	}else {
    		parkingLotManager.readInputFromConsole();
    	}  	
    } 
}
